import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.Text;
import org.jdom.filter.ElementFilter;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 * 
 * @author aeast
 *
 *	Changes:
 * 		04-25-2-11 - APE - Added mm_head__photo processing
 * 
 */
public class Propagator {

	private boolean debug = false;
	
	public static void main(String[] args) {
		// main for testing purposes
		if (args == null || args.length == 0) {
			args = new String[1];
			args[0] = new String("/temp/xml/realSample.xml");
		}
		
		//System.out.println("Default encoding: " + Charset.defaultCharset().displayName());
		
		Propagator p = new Propagator();
				
		p.doit(args);
		
		
	}

	/**
	 * 
	 * @param args  args[0] should contain file name of file to be processed.
	 * @return 0, if successful, otherwise -1
	 */
	public int doit(String[] args) {
		int returnCode = 0;
		
		//System.out.println("Propagator Running : ");
		
		// Change to false for production use
		//this.setDebug(true);
			
		returnCode = this.doTransformations(args);
		
		//System.out.println("Result of execution: " + returnCode);
		
		return returnCode;
	}
	
	/**
	 * 
	 * @param args
	 * @return
	 */
	private int doTransformations(String[] args) {
		int returnCode = 0;

		try {
			SAXBuilder b = new SAXBuilder();
			String file1 = args[0];
			File f =  new File(file1);
			if (!f.exists()) {
				System.out.println("Propagator Macro: Input File not Found '" + f.getAbsolutePath() + "'");
				return -1;
			}
			Document doc1 = b.build(f);

			String timeStr = "";
			String originalFileName = "";
			String debugPath = "/net/devngclio.eng.gci/ngiohome/ngio/Catch/";
			//String debugPath = "/ngiohome/ngio/Catch/";
			if (this.isDebug()) {
				try {
					//this.dumpToSysOut(doc1);
					timeStr = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + "_" + Calendar.getInstance().get(Calendar.MINUTE) + "_" + Calendar.getInstance().get(Calendar.SECOND);
					originalFileName = f.getName();
					originalFileName = originalFileName.substring(0, (originalFileName.length()-4));
					String file2 = debugPath + originalFileName + "_PropagatorInputXML_" + timeStr + ".xml";
					this.writeOutput(doc1, file2);
				}
				catch (Exception exp) {
					exp.printStackTrace();
					System.out.println("Propagator Macro: " + exp.getMessage());
				}
			}			
			Namespace ns = org.jdom.Namespace.getNamespace("urn:schemas-ccieurope.com");

			Element head = null;
			Element mm_head = null;
			Element mm_headdeck = null;
			Element mm_summary_chatter = null;
			Element mm_summary = null;
			Element byline = null;
			Element body = null;
			Element mm_head_photo = null;
			
			head = this.getFirstXMLElementWithName(doc1, "head");
			mm_head = this.getFirstXMLElementWithName(doc1, "mm_head");
			mm_headdeck = this.getFirstXMLElementWithName(doc1, "mm_headdeck");
			mm_head_photo = this.getFirstXMLElementWithName(doc1, "mm_head__photo");
			mm_summary_chatter = this.getFirstXMLElementWithName(doc1, "mm_summary__chatter");
			mm_summary = this.getFirstXMLElementWithName(doc1, "mm_summary");
			byline = this.getFirstXMLElementWithName(doc1, "byline");
			body = this.getFirstXMLElementWithName(doc1, "body");

			//
			// Note, order is important since the mm_head and mm_headdeck processing is dependent
			// on the processing of the headline. Headline must be processed first.
			//
			// UPDATE 05/13/2011 - No longer want head to have count. NO longer processing head, so, 
			// 					   need to call countTextAndAppend within methods that use head element.
			//
			
			// process headline   (not as of 05/13/2011)
			//head = this.processHeadline(head);

			// Find first paragraph
			String firstParagraph = this.getFirstParagraphOfBody(body);
			
			// process the head photo tag
			mm_head_photo = this.processHeadPhoto(mm_head_photo);
			
			// copy head to mm_head if mm_head tag set is empty otherwise just count characters
			mm_head = this.processMMHead(mm_head, head);
						
			// copy head to mm_headdeck if mm_headdeck tag set is empty otherwise count characters
			mm_headdeck = this.processMMHead(mm_headdeck, head);

			// count mm_summary
			mm_summary = this.processSummary(mm_summary, firstParagraph, ns);
			
			// process/count mm_summary__chatter
			mm_summary_chatter = this.processSummary(mm_summary_chatter, firstParagraph, ns);
						
			// modify case of byline
			byline = this.processByline(byline);
			
			// Write output files
			//String file2 = "c:/temp/xml/realSample2_Output.xml";
			String file2 = file1;
			returnCode = this.writeOutput(doc1, file2);
			
			if (this.isDebug()) {
				try {
					//this.dumpToSysOut(doc1);
					file2 = debugPath + originalFileName + "_PropagatorOutputXML_" + timeStr + ".xml";
					this.writeOutput(doc1, file2);
				}
				catch (Exception exp) {
					exp.printStackTrace();
					System.out.println(exp.getMessage());
				}
			}
			
		
		} catch (IOException e) {
			e.printStackTrace();
			returnCode = -1;
		} catch (JDOMException e) {
			e.printStackTrace();
			returnCode = -1;
		}
		catch (Exception e) {
			e.printStackTrace();
			returnCode = -1;
		}
		return returnCode;
		
	}
	
	/**
	 * 
	 * @param doc
	 * @param elementName
	 * @return the first instance of the requested element
	 */
	@SuppressWarnings({ "rawtypes" })
	private Element getFirstXMLElementWithName(Document doc, String elementName) {
		Element e = null;
		Iterator i = null;
		i = doc.getDescendants(new ElementFilter(elementName));
		if (i.hasNext()) {
			e = (Element)i.next();
		}		
		return e;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	private String stripPreviousBrackets(String source) {
		if (source != null && source.startsWith("{")) {
			int lastIndex = source.indexOf("}");
			if (lastIndex > -1 && lastIndex < 10) {
				if (lastIndex < source.length()) {
					// remove brackets
					source = source.substring((lastIndex+1));
				}
			}
		}
		return source;
	}
	/**
	 * 
	 * @param body
	 * @return  the String representation of the first paragraph contents
	 */
	@SuppressWarnings({ "rawtypes" })
	private String getFirstParagraphOfBody(Element body) {
		String paragraph = "";
		if (body != null) {
			Iterator itr = body.getContent().iterator();			
			Text childText = null;
			while (itr.hasNext()) {
				Object o = itr.next();
				if (o instanceof Text) {
					childText = (Text)o;
				}
				else {
					// could be a note or other annotation
					continue;
				}
				
				if (childText.getParent().equals(body)) {
					// only operate on one level deep text
					break;
				}
				
			} // end while more content
			
			if (childText != null) {
				String text = childText.getText();

				if (text == null) {
					text = "";
				}
				
				int indexOfEnDash = text.indexOf('�');
				
				// if end-dash not within the first 500, then ignore it
				if (indexOfEnDash > -1 && indexOfEnDash < 500) {
					paragraph = text.substring((indexOfEnDash+1));
					paragraph = paragraph.trim();
				}
				else {
					paragraph = text;
				}
			}
		}
		
		return paragraph;
	}
	
	/**
	 * 
	 * @param head return the modified head
	 * @return the headline element that was passed into the method
	 */
	@SuppressWarnings("unused")
	private Element processHeadline(Element head) {
		
		try {
			// we need only count the chars in the headline.
			return this.countTextAndAppend(head);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return head;
	}
	
	private Element processHeadPhoto(Element headPhoto) {
		
		try {
			// we need only count the chars in the headline.
			return this.countTextAndAppend(headPhoto);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return headPhoto;
	}
	
	/**
	 * 
	 * @param mmElement
	 * @param headline
	 * @return the element being processed
	 */

	private Element processMMHead(Element mmElement, Element headline) {
		if (mmElement != null) {
			@SuppressWarnings("rawtypes")
			List mm_headChildren = mmElement.getContent();
			if (mm_headChildren.size() == 0) {
				if (headline != null) {
					mmElement.addContent(headline.cloneContent());
					
					// APE - 05/13/2011 - added this call to count text since the head
					///                   no longer contains the count.
					mmElement = this.countTextAndAppend(mmElement);
				}
			} // otherwise element already has content
			else {
				// just text
				mmElement = this.countTextAndAppend(mmElement);
				
			}
		} // end if element != null

		return mmElement;
	}
	/**
	 * 
	 * @param summary
	 * @param firstParagraph
	 * @param ns
	 * @return
	 */
	private Element processSummary(Element summary, String firstParagraph, Namespace ns) {
		// count mm_summary_chatter
		StringBuilder textBuilder = new StringBuilder();
		
		if (summary != null) {
			String textToUse = firstParagraph;
			Text mm_Child = null;
			// simply count and replace
			@SuppressWarnings("rawtypes")
			List mm_headSummaryChildren = summary.getContent(); 
			if (mm_headSummaryChildren.size() != 0) {
				@SuppressWarnings("rawtypes")
				Iterator ii = mm_headSummaryChildren.iterator();
				while (ii.hasNext()) {
					Object o = ii.next();
					
					if (o instanceof Text) {
						Text t = (Text)o;
						if (mm_Child == null) {
							mm_Child = t;
						}
						textToUse = t.getText();
						
						if ((textToUse != null && textToUse.trim().length() > 0)) {
							textBuilder.append(textToUse);							
						}
					} // end if Text
				}
			}
			
			if (textBuilder.toString().trim().length() == 0) {
				if (mm_Child != null) {
					mm_Child.setText(firstParagraph);
				}
				else {
					summary.setText(firstParagraph);
				}
			}

			summary = this.countTextAndAppend(summary);
		}
		return summary;
	}
	
	/**
	 * 
	 * @param doc
	 * @param fileName
	 * @return 0, if successful, otherwise -1
	 */
	private int writeOutput(Document doc, String fileName) {
		int returnCode = 0;
		try {
			Charset utf8 = Charset.forName("UTF-8");
			
			Format formatter = Format.getRawFormat();
			formatter.setEncoding(utf8.name());
	
			File outputFile = new File(fileName);
			OutputStreamWriter owriter = new OutputStreamWriter(new FileOutputStream(outputFile), utf8);
				      			
			XMLOutputter outputter = new XMLOutputter(formatter);
			
			outputter.output(doc, owriter);
			owriter.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			returnCode = -1;
		}
		return returnCode;
	}

	/**
	 * 
	 * @param doc
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unused")
	private void dumpToSysOut(Document doc) {
		try {
			System.out.println("Propagator Macro: Dumping xml...");
			System.out.println();System.out.println();System.out.println();
			
			Charset utf8 = Charset.forName("UTF-8");
			
			Format formatter = Format.getRawFormat();
			formatter.setEncoding(utf8.name());
	
			OutputStreamWriter owriter = new OutputStreamWriter(System.out, utf8);
				      			
			XMLOutputter outputter = new XMLOutputter(formatter);
			
			outputter.output(doc, owriter);
			
			System.out.println();System.out.println();System.out.println();
			System.out.println("Propagator Macro: Done dumping xml.");System.out.println();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param input
	 * @return mixed case string 
	 *   
	 *   This version works on multiple work lines but fails in the case of the name including something like "McCloud"
	 *   as a result we are using V2 which splits the multiple line into individual tokens and then determines what to do.
	 */
	public static String toMixed(String input) {
		String response = input;
		if (input != null) {
	        StringBuffer buff = new StringBuffer(input.length());   
	        boolean isWordStarted = false;   
	        for (int i = 0; i < input.length(); i++) {   
	            char c = input.charAt(i);   
	            if (Character.isLetter(c)) {   
	                if (isWordStarted) {  
	                    c = Character.toLowerCase(c);
	                }
	                else { 
	                	// we don't want to upper case anything
	                    // c = Character.toUpperCase(c);
	                	;
	                }
	                isWordStarted = true;   
	            } 
	            else {   
	                isWordStarted = false;   
	            }   
	            buff.append(c);   
	        } // end for
	        response = buff.toString();
		}
        return response;   
    }  

	/**
	 * 
	 * @param input
	 * @return
	 * 
	 * This version slits the input name into tokens and then mix cases them as needed.
	 */
	public static String toMixedV2(String input) {
		StringTokenizer tokenizer = new StringTokenizer(input);

		StringBuilder nameBuilder = new StringBuilder();
		
		while (tokenizer.hasMoreTokens()) {
			nameBuilder.append(toMixedV2a(tokenizer.nextToken()));
			if (tokenizer.hasMoreTokens()) {
				nameBuilder.append(" ");
			}
		}
		
		return nameBuilder.toString();
	}
	

	/**
	 * 
	 * @param input
	 * @return
	 * 
	 * The mixed case version of the input if it started out all caps, 
	 * 
	 */
	public static String toMixedV2a(String input) {
		
		//System.out.println("Processing String: " + input);
		
		String response = input;
		boolean mixedCase = false;
		boolean containsUpperCase = false;
		boolean containsLowerCase = false;
		int upperCaseCount = 0;
		if (input != null) {
	        for (int j = 0; j < input.length(); j++) {
	            char c = input.charAt(j);  
	            if (Character.isLetter(c)) { 
		            if (Character.isUpperCase(c)) {
		            	containsUpperCase = true;
		            	upperCaseCount++;
		            }
		            else {
		            	if (Character.isLowerCase(c)){
		            		containsLowerCase = true;
		            	}
		            }
	            }
	        }
	        if (containsLowerCase && containsUpperCase) {
	        	mixedCase = true;
	        }       
	        
	        //add check here to just return if mixed case and upper case count < 3
	        if (mixedCase && (upperCaseCount < 3)) {
	        	// for names lick McCloud.
	        	// skip em
	        	return input;
	        }
	        
	        StringBuffer buff = new StringBuffer(input.length());   
	        boolean isWordStarted = false;   
	        for (int i = 0; i < input.length(); i++) {   
	            char c = input.charAt(i);   
	            if (Character.isLetter(c)) {   
	                if (isWordStarted) {  
                		c = Character.toLowerCase(c);
	                }
	                else { 
	                	// we don't want to upper case anything
	                    // c = Character.toUpperCase(c);
	                	;
	                }
	                isWordStarted = true;   
	            } 
	            else {   
	                isWordStarted = false;   
	            }   
	            buff.append(c);   
	        } // end for
	        response = buff.toString();
		}
        return response;   
    }  
	
	/**
	 * 
	 * @param byline
	 * @return byline
	 */
	@SuppressWarnings({ "rawtypes" })
	private Element processByline(Element byline) {
		if (byline != null) {
			
			List children = byline.getContent();
			if (children.size() == 0) {
				// do nothing 
				return byline;
			}
			
			Iterator ii = children.iterator();
			Text theText = null;
			String originalValue = null;
			while (ii.hasNext()) {
				Object o = ii.next();
				if (o instanceof Text) {
					theText = (Text)o;
					originalValue = theText.getText();
					break;
				}
			}
			
			if (theText != null && originalValue != null ) {

				int indexOfUSATODAY = originalValue.indexOf("USA TODAY");
				int indexOfComma = originalValue.indexOf(",");
				
				boolean containsUSAT = (indexOfUSATODAY > -1) ? true : false;
				boolean containsComma = (indexOfComma > -1) ? true : false;
				
				if (containsComma) {
					
					String name = originalValue.substring(0,indexOfComma);
					
					if (containsUSAT) {
						// only do this if USA TODAY not in this part
						if (indexOfUSATODAY > indexOfComma) {
							//name = Propagator.toMixed(name);
							name  = Propagator.toMixedV2(name);
						}
					}
					else {
						//name  = Propagator.toMixed(name);
						name  = Propagator.toMixedV2(name);
					}
					
					String remainder = originalValue.substring(indexOfComma);
					
					theText.setText(name+remainder);
				}
				else {
					if (containsUSAT) {
						String part1 = originalValue.substring(0,indexOfUSATODAY);
						String part2 = originalValue.substring(indexOfUSATODAY);
						//part1 = Propagator.toMixed(part1);
						part1  = Propagator.toMixedV2(part1);
						
						theText.setText(part1 + part2);
					}
					else {
						//String part1 = Propagator.toMixed(originalValue);
						String part1 = Propagator.toMixedV2(originalValue);
						theText.setText(part1);
					}
				}
			}
			
		}
		return byline;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	@SuppressWarnings("rawtypes")
	private Element countTextAndAppend(Element source) {
		String originalText = null;
		Text newTextObj = null;
		StringBuilder textBuilder = new StringBuilder();
		
		if (source != null) {
			// count characters in head
			List contentList = source.getContent();

			Iterator itr = contentList.iterator();

			if (itr == null || !itr.hasNext()) {
				// simply return head
				return source;
			}
			
			while (itr.hasNext()) {
				Object o = itr.next();
				//System.out.print("Object is of type: " + o.getClass().getName());
				if (o instanceof Text) {
					Text t = (Text)o;
					if (newTextObj == null) {
						newTextObj = t;  // we will update this one with the count later
					}
//					System.out.println(" Value: " + t.getValue());
					textBuilder.append(t.getValue());
				}
				// following for debugging only
//				else if (o instanceof Element) {
//					Element e = (Element)o;
//					System.out.println(" Name: " + e.getName() + "  Value: " + e.getText());					
//				}
			}
			originalText = textBuilder.toString();
			
			
			if (originalText == null) {
				originalText = "";
			}
			
			// check if this story has already been processed with this macro by checking
			// for the existence of the bracket in position 0
			originalText = this.stripPreviousBrackets(originalText);
			
			StringBuilder newText = new StringBuilder();
			newText.append("{").append(originalText.length()).append("}");
			String firstTextStripped = this.stripPreviousBrackets(newTextObj.getText());
			newText.append(firstTextStripped);
			if (newTextObj != null) {
				newTextObj.setText(newText.toString());
			}
		}
		return source;
		
	}
}
