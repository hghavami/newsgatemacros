
import java.net.URL;
import java.net.URLClassLoader;

import org.jdom.input.SAXBuilder;


public class PrintClasspath {

	/**
	 * @param args
	 */

    public static void main(String[] args) {

        //Get the System Classloader
        ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();
        
        URLClassLoader urlcl = (URLClassLoader)sysClassLoader;
        
        String cpath = System.getenv("CLASSPATH");
        
        System.out.println("classpath: " + cpath);
        
        SAXBuilder sb = null;
        
        //Get the URLs
        URL[] urls = ((URLClassLoader)sysClassLoader).getURLs();

        for(int i=0; i< urls.length; i++)
        {
            System.out.println(urls[i].getFile());
        }       

    }
}
